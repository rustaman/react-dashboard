# React Dashboard

This is a React Admin Dashboard learning project.

I have done it following [this tutorial](https://www.youtube.com/watch?v=fq7k_gVV5x8)

Project deployed [here](https://react-admin-dashboard-53066.web.app/)
