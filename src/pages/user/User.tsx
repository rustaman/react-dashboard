import Single from '../../components/single/Single';
import { useParams } from 'react-router-dom';
import './User.scss';
import { userRows, singleUser } from '../../data';

const User = () => {
  const params = useParams();
  const id: number = parseInt(params.id!);
  const user = userRows.find(user => user.id === id);

  const userInfo = {
    firstName: user?.firstName,
    lastName: user?.lastName,
    email: user?.email,
    phone: user?.phone,
    createdAt: user?.createdAt,
    verified: user?.verified ? 'YES' : 'NO',
  };

  console.log(params);
  return (
    <div className="user">
      {user && (
        <Single
          id={id}
          img={user.img}
          title={`${user.firstName} ${user.lastName}`}
          info={userInfo}
          chart={singleUser.chart}
          activities={singleUser.activities}
        />
      )}
    </div>
  );
};

export default User;
