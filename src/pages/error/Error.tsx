import Navbar from '../../components/navbar/Navbar';
import Footer from '../../components/footer/Footer';
import Menu from '../../components/menu/Menu';

const Error = () => {
  return (
    <div className="main">
      <Navbar />
      <div className="container">
        <div className="menuContainer">
          <Menu />
        </div>
        <div className="contentContainer">
          <h3 className="error">This page does not exists (yet).</h3>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Error;
