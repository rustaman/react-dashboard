import Single from '../../components/single/Single';
import './Product.scss';
import { useParams } from 'react-router-dom';
import { products, singleProduct } from '../../data';

const Product = () => {
  // Fetch data and send to Single Component
  const params = useParams();
  const id: number = parseInt(params.id!);
  const product = products.find(product => product.id === id);

  const productInfo = {
    title: product?.title,
    color: product?.color,
    producer: product?.producer,
    price: product?.price,
    createdAt: product?.createdAt,
    inStock: product?.inStock ? 'YES' : 'NO',
  };

  console.log(params);
  return (
    <div className="user">
      {product && (
        <Single
          id={id}
          img={product.img}
          title={product.title}
          info={productInfo}
          chart={singleProduct.chart}
          activities={singleProduct.activities}
        />
      )}
    </div>
  );
};

export default Product;
