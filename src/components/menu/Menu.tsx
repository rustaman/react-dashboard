import { Link } from 'react-router-dom';
import './Menu.scss';
import { menu } from '../../data';

const Menu = () => {
  return (
    <div className="menu">
      {menu.map(item => {
        return (
          <div key={item.id} className="item">
            <span className="title">{item.title}</span>

            {item.listItems.map(link => {
              return (
                <Link key={link.id} className="listItem" to={link.url}>
                  <img src={link.icon} alt="" />
                  <span className="listItemTitle">{link.title}</span>
                </Link>
              );
            })}
          </div>
        );
      })}
    </div>
  );
};

export default Menu;
