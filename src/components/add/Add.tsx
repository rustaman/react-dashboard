import { GridColDef } from '@mui/x-data-grid';
import './Add.scss';

type Props = {
  slug: string;
  columns: GridColDef[];
  setShow: React.Dispatch<React.SetStateAction<boolean>>;
};

const fieldsToExclude = ['id', 'img', 'verified'];

const Add = (props: Props) => {
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
  };
  return (
    <div className="add">
      <div className="modal">
        <span className="close" onClick={() => props.setShow(false)}>
          X
        </span>
        <h1>Add new {props.slug}</h1>
        <form onSubmit={handleSubmit}>
          {props.columns
            .filter(column => !fieldsToExclude.includes(column.field))
            .map(column => {
              return (
                <div key={column.field} className="item">
                  <label htmlFor={column.field}>{column.headerName}</label>
                  <input
                    type={column.type}
                    name={column.field}
                    id={column.field}
                    placeholder={column.headerName}
                  />
                </div>
              );
            })}
          <button type="submit">Send</button>
        </form>
      </div>
    </div>
  );
};

export default Add;
